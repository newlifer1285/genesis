build: docker composer migrations fixtures test

docker:
		docker-compose -f docker-compose.yml build
		docker-compose -f docker-compose.yml up -d
		docker-compose ps
composer:
		composer update

migrations:
		docker-compose exec -T php yii migrate/up --interactive=0

fixtures:
		docker-compose exec -T php yii fixture/load Post,Category --interactive=0

test:
		docker-compose exec -T php codecept run unit