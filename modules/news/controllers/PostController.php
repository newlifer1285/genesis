<?php

namespace app\modules\news\controllers;

use app\modules\news\models\Post;

/**
 * PostController implements the REST for Post model.
 */

/**
 * @SWG\Get(path="/posts",
 *     tags={"Post"},
 *     summary="Retrieves the collection of Post resources.",
 *     @SWG\Response(
 *         response = 200,
 *         description = "Post collection response",
 *         @SWG\Schema(ref = "#/definitions/Post")
 *     ),
 * )
 *
 * @method actionIndex
 *
 * @SWG\Post(path="/post",
 *     tags={"Post"},
 *     summary="Create Post resource.",
 *     @SWG\Response(
 *         response = 200,
 *         description = "Post entity response",
 *         @SWG\Schema(ref = "#/definitions/Post")
 *     ),
 * )
 *
 * @method actionCreate
 *
 *
 * @SWG\Put(path="/post/{id}",
 *     tags={"Post"},
 *     summary="Update Post resource.",
 *     @SWG\Parameter(name="id",in="path",type="integer",description="Post id."),
 *     @SWG\Response(
 *         response = 200,
 *         description = "Post entity response",
 *         @SWG\Schema(ref = "#/definitions/Post")
 *     ),
 * )
 *
 * @method actionUpdate
 *
 * @SWG\Delete(path="/post/{id}",
 *     tags={"Post"},
 *     summary="Delete Post resource.",
 *     @SWG\Parameter(name="id",in="path",type="integer",description="Post id."),
 *     @SWG\Response(
 *         response = 204,
 *         description = "Delete Post resource",
 *         @SWG\Schema(ref = "#/definitions/Post")
 *     ),
 * )
 *
 * @method actionDelete
 */
class PostController extends ActiveCacheController
{
    /**
     * @var string Post class name.
     */
    public $modelClass = Post::class;
}