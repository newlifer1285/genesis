<?php

namespace app\modules\news\controllers;

use app\modules\news\actions\CreateAction;
use app\modules\news\actions\DeleteAction;
use app\modules\news\actions\IndexAction;
use app\modules\news\actions\UpdateAction;
use app\modules\news\actions\ViewAction;
use yii\rest\ActiveController;

/**
 * Class ActiveCacheController
 *
 * @package app\modules\news\controllers
 */
class ActiveCacheController extends ActiveController
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => ViewAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->updateScenario,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}
