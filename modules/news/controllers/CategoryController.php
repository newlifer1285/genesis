<?php

namespace app\modules\news\controllers;

use app\modules\news\models\Category;

/**
 * CategoryController implements the REST for Category model.
 */

/**
 * @SWG\Get(path="/categories",
 *     tags={"Category"},
 *     summary="Retrieves the collection of Category resources.",
 *     @SWG\Response(
 *         response = 200,
 *         description = "Category collection response",
 *         @SWG\Schema(ref = "#/definitions/Category")
 *     ),
 * )
 *
 * @method actionIndex
 */
class CategoryController extends ActiveCacheController
{
    /**
     * @var string Category class name.
     */
    public $modelClass = Category::class;
}
