<?php

namespace app\modules\news\controllers;

use yii\helpers\Url;
use yii\web\Controller;

/**
 * @SWG\Swagger(
 *     basePath="/",
 *     produces={"application/json"},
 *     consumes={"application/x-www-form-urlencoded"},
 *     @SWG\Info(version="2.0", title="News API"),
 * )
 */
class ApiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'docs' => [
                'class' => 'yii2mod\swagger\SwaggerUIRenderer',
                'restUrl' => Url::to(['/']),
            ],
            'index' => [
                'class' => 'yii2mod\swagger\OpenAPIRenderer',
                'scanDir' => [
                    \Yii::getAlias('@app/modules/news/controllers'),
                    \Yii::getAlias('@app/modules/news/models'),
                ],
            ],
        ];
    }

}
