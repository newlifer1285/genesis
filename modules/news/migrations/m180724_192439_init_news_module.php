<?php
namespace app\modules\news\migrations;

use yii\db\Migration;

/**
 * Class m180724_192439_init_news_module
 */
class m180724_192439_init_news_module extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{category}}', [
            'id' => $this->primaryKey(5),
            'name' => $this->string(155),
            'posts_counter' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createTable('{{post}}', [
            'id' => $this->primaryKey(11),
            'title' => $this->string(155)->notNull(),
            'body' => $this->text()->notNull(),
            'category_id' => $this->integer(5)->notNull(),
        ]);

        $this->addForeignKey('fk_category_id', '{{post}}', 'category_id', '{{category}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{post}}');
        $this->dropTable('{{category}}');
    }
}
