<?php

namespace app\modules\news\queries;

/**
 * This is the ActiveQuery class for [[\app\modules\news\models\Category]].
 *
 * @see \app\modules\news\models\Category
 */
class CategoryQuery extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \app\modules\news\models\Category[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\news\models\Category|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);

    }
}
