<?php

namespace app\modules\news\queries;

/**
 * This is the ActiveQuery class for [[\app\modules\news\models\Post]].
 *
 * @see \app\modules\news\models\Post
 */
class PostQuery extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \app\modules\news\models\Post[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\news\models\Post|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
