<?php

namespace app\modules\news\models;

use app\modules\news\queries\PostQuery;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int $category_id
 *
 * @property Category $category
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'body', 'category_id'], 'required'],
            [['body'], 'string'],
            [['category_id'], 'integer'],
            [['title'], 'string', 'max' => 155],
            [['category_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Category::class,
                'targetAttribute' => ['category_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\news\queries\PostQuery
     *          the active query used by this AR class.
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function afterDelete()
    {
        $this->category->decrementPostCounter();

        parent::afterDelete();
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->category->incrementPostCounter();
        } elseif (!empty($changedAttributes['category_id'])) {
            $changedCategoryId = $changedAttributes['category_id'];

            if ($this->category_id != $changedCategoryId) {
                $oldCategory = Category::findOne($changedCategoryId);

                $oldCategory->decrementPostCounter();
                $this->category->incrementPostCounter();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }
}
