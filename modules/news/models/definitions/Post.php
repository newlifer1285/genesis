<?php
namespace app\modules\news\models\definitions;

/**
 * @SWG\Definition(required={"title", "body", "category_id"})
 *
 * @SWG\Property(property="id", type="integer", example=1)
 * @SWG\Property(property="title", type="string", example="The best post title ever")
 * @SWG\Property(property="body", type="string", example="Body post")
 * @SWG\Property(property="category_id", type="integer", example=1)
 *
 */
class Post
{

}