<?php
namespace app\modules\news\models\definitions;

/**
 * @SWG\Definition(required={"name"})
 *
 * @SWG\Property(property="id", type="integer", example=1)
 * @SWG\Property(property="name", type="string", example="Main category")
 * @SWG\Property(property="posts_counter", type="integer", example=1)
 */
class Category
{

}