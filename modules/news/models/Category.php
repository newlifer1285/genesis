<?php

namespace app\modules\news\models;

use app\modules\news\helpers\CacheTags;
use app\modules\news\queries\CategoryQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property int $posts_counter
 *
 * @property Post[] $posts
 */
class Category extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['posts_counter'], 'integer'],
            [['name'], 'string', 'max' => 155],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'posts_counter' => 'Posts Counter',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::class, ['category_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\news\queries\CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * Update counters.
     *
     * @param array $counters Counters data array.
     *
     * @return bool
     */
    public function updateCounters($counters)
    {
        $result = parent::updateCounters($counters);

        \Yii::$app->cacheTagManager->invalidate(\Yii::$app->cache, CacheTags::CATEGORIES . $this->id);
        \Yii::$app->cacheTagManager->invalidate(\Yii::$app->cache, CacheTags::CATEGORIES);

        return $result;
    }

    /**
     * Increment posts counter.
     *
     * @return void
     */
    public function incrementPostCounter()
    {
        $this->updateCounters(['posts_counter' => 1]);
    }


    /**
     * Decrement posts counter.
     *
     * @return void
     */
    public function decrementPostCounter()
    {
        $this->updateCounters(['posts_counter' => -1]);
    }
}
