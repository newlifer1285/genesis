<?php

namespace app\modules\news\actions;

/**
 * Class IndexAction
 *
 * @package app\modules\news\actions
 */
class IndexAction extends \yii\rest\IndexAction
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return \Yii::$app->cache->getOrSet($this->modelClass, function($db) {
            return parent::run();
        }, null, \Yii::$app->cacheTagManager->createDependency(['tags' => $this->modelClass]));
    }
}