<?php
namespace app\modules\news\actions;

/**
 * Class CreateAction
 *
 * @package app\modules\news\actions
 */
class CreateAction extends \yii\rest\CreateAction
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        \Yii::$app->cacheTagManager->invalidate(\Yii::$app->cache, $this->modelClass);

        return parent::run();
    }
}
