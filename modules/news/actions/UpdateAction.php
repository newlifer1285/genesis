<?php

namespace app\modules\news\actions;

/**
 * Class UpdateAction
 *
 * @package app\modules\news\actions
 */
class UpdateAction extends \yii\rest\UpdateAction
{
    /**
     * {@inheritdoc}
     */
    public function run($id)
    {
        $key = $this->modelClass . $id;

        \Yii::$app->cacheTagManager->invalidate(\Yii::$app->cache, $key);

        return \Yii::$app->cache->getOrSet($key, function($db) use ($id) {
            return parent::run($id);
        }, null, \Yii::$app->cacheTagManager->createDependency(['tags' => $key]));
    }
}