<?php

namespace app\modules\news\actions;

/**
 * Class ViewAction
 *
 * @package app\modules\news\actions
 */
class ViewAction extends \yii\rest\ViewAction
{
    /**
     * {@inheritdoc}
     */
    public function run($id)
    {
        $key = $this->modelClass . $id;

        return \Yii::$app->cache->getOrSet($key, function($db) use ($id) {
            return parent::run($id);
        }, null, \Yii::$app->cacheTagManager->createDependency(['tags' => $key]));
    }
}