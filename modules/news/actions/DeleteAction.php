<?php
namespace app\modules\news\actions;

/**
 * Class DeleteAction
 *
 * @package app\modules\news\actions
 */
class DeleteAction extends \yii\rest\DeleteAction
{
    /**
     * {@inheritdoc}
     */
    public function run($id)
    {
        \Yii::$app->cacheTagManager->invalidate(\Yii::$app->cache, $this->modelClass . $id);

        parent::run($id);
    }
}