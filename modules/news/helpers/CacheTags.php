<?php
namespace app\modules\news\helpers;

use app\modules\news\models\Category;
use app\modules\news\models\Post;

/**
 * Class CacheTags
 *
 * @package app\modules\news\helpers
 */
class CacheTags
{
    public const CATEGORIES = Category::class;
    public const POSTS = Post::class;
}