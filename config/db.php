<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=gen_db;dbname=news_db',
    'username' => 'root',
    'password' => 'mysecretpassword',
    'charset' => 'utf8',
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
