<?php

namespace app\tests\fixtures;

use app\modules\news\models\Category;
use yii\test\ActiveFixture;

class CategoryFixture extends ActiveFixture
{
    public $modelClass = Category::class;
    public $dataFile = '@app/tests/_data/category.php';
}