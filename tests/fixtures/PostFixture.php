<?php

namespace app\tests\fixtures;

use app\modules\news\models\Category;
use app\modules\news\models\Post;
use yii\test\ActiveFixture;

class PostFixture extends ActiveFixture
{
    public $modelClass = Post::class;
    public $depends = [CategoryFixture::class];
    public $dataFile = '@app/tests/_data/post.php';
}