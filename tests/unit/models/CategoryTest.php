<?php

namespace tests\models;


use app\modules\news\models\Category;
use app\modules\news\models\Post;
use app\tests\fixtures\CategoryFixture;
use app\tests\fixtures\PostFixture;
use Codeception\Test\Unit;

class CategoryTest extends Unit
{
    public function _fixtures()
    {
        return [
            'categories' => [
                'class' =>  CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
        ];
    }

    public function testIncrementPostsCounter()
    {
        $category = Category::find()->one();

        $oldPostsCounter = $category->posts_counter;

        $post = new Post([
            'title' => 'post_new',
            'body' => 'the best body text ever',
            'category_id' => $category->id
        ]);

        $post->save();

        $category = Category::findOne($category);

        expect($category->posts_counter)->equals($oldPostsCounter + 1);
    }

    public function testDecrementPostsCounter()
    {
        $post = Post::find()->one();

        $currentCategory = $post->category;
        $nextCategory = Category::find()->where('id <> :id', [':id' => $currentCategory->id])->one();

        $oldPostsCounter = $currentCategory->posts_counter;

        $post->category_id = $nextCategory->id;
        $post->save();

        $currentCategory->refresh();

        expect($currentCategory->posts_counter)->equals($oldPostsCounter - 1);
    }
}