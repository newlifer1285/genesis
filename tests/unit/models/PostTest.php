<?php

namespace tests\models;


use app\modules\news\models\Category;
use app\modules\news\models\Post;
use app\tests\fixtures\CategoryFixture;
use app\tests\fixtures\PostFixture;
use Codeception\Test\Unit;

class PostTest extends Unit
{
    public function _fixtures()
    {
        return [
            'categories' => [
                'class' =>  CategoryFixture::class,
                'dataFile' => codecept_data_dir() . 'category.php'
            ],
            'posts' => [
                'class' =>  PostFixture::class,
                'dataFile' => codecept_data_dir() . 'post.php'
            ],
        ];
    }

    public function testCreatePostSuccess()
    {
        $category = Category::find()->one();

        $post = new Post([
            'title' => 'post_new',
            'body' => 'the best body text ever',
            'category_id' => $category->id
        ]);

        expect_that($post->save() === true);
    }

    public function testUpdatePostSuccess()
    {
        $post = Post::find()->one();
        $category = Category::findOne(2);

        $post->title = 'new post title';
        $post->body = 'new post body';
        $post->category_id = $category->id;
        $post->save();

        expect($post->title)->equals('new post title');
        expect($post->body)->equals('new post body');
        expect($post->category_id)->equals($category->id);
    }

    public function testDeletePostSuccess()
    {
        $post = Post::find()->one();
        $postId = $post->id;

        $post->delete();

        expect(Post::findOne($postId))->null();
    }

    public function testValidationSuccess()
    {
        $category = Category::find()->one();

        $post = new Post([
            'title' => 'post_new',
            'body' => 'the best body text ever',
            'category_id' => $category->id
        ]);

        expect_that($post->validate() === true);
    }

    public function testValidationCategoryFailure()
    {
        $post = new Post([
            'title' => 'post_new',
            'body' => 'the best body text ever',
        ]);

        expect($post->validate())->false();

        expect($post->getErrors('category_id'))->notEmpty();
    }
}