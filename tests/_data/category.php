<?php
return [
    'category1' => [
        'id' => 1,
        'name' => 'Default',
        'posts_counter' => 1,
    ],
    'category2' => [
        'id' => 2,
        'name' => 'Main',
        'posts_counter' => 1,
    ],
];