<?php
return [
    'post1' => [
        'id' => 1,
        'title' => 'post_1',
        'body' => 'post_1 body',
        'category_id' => 1,
    ],
    'post2' => [
        'id' => 2,
        'title' => 'post_2',
        'body' => 'post_2 body',
        'category_id' => 2,
    ],
];