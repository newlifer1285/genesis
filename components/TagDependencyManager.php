<?php
namespace app\components;

use yii\base\Component;
use yii\caching\Cache;
use yii\caching\TagDependency;

/**
 * Class TagDependencyManager
 *
 * @package app\components
 */
class TagDependencyManager extends Component
{
    /**
     * @var TagDependency
     */
    public $dependencyClass;

    /**
     * Invalidate cache.
     *
     * @param Cache $cache Cache.
     *
     * @param array|string $tags Tags.
     */
    public function invalidate(Cache $cache, $tags):void
    {
        $this->dependencyClass::invalidate($cache, $tags);
    }

    /**
     * Create dependency object.
     *
     * @param array $config Config.
     *
     * @return TagDependency
     */
    public function createDependency(array $config)
    {
        return new $this->dependencyClass($config);
    }
}