### Документация

~~~
http://127.0.0.1:8000/docs
~~~

### Сборка проекта

~~~
make
~~~

### Что можно было бы сделать иначе:

1. Вынести в .env конфиги для разных окружений.
2. Переопределить yii\rest\DeleteAction и возвращать тело response, например id удаленной записи, если она удалена.
3. Весто yii\rest использовать обычные контроллеры и отдавать в GET /categories, например:
 ```$this->asJson(Category::find()->asArray()->all());``` 
4. Сделать кеширование данных в бихевиор или ActiveQuery перед отдачей клиенту, но нужно не забыть об инвалидации.
5. Замокать компонент cache в тестах.
6. Создать отдельную базу для тестов.


### Как проверить работоспособность:

~~~
curl -X GET -H 'Accept: application/json; q=1.0, */*; q=0.1'   http://127.0.0.1:8000/categories

curl -X PUT -d "category_id=2"  http://127.0.0.1:8000/post/1

curl -X DELETE  http://127.0.0.1:8000/post/1

curl -X GET http://127.0.0.1:8000/posts

curl -X POST -d "title=post_title&body=post_body&category_id=1" http://127.0.0.1:8000/post
~~~


**FYI**

- если проверять в браузере, нужен хидер "Accept: application/json; q=1.0, */*; q=0.1"

### Запуск тестов:

~~~
    make test
~~~